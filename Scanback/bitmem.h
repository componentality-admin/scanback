/****************************************************************************/
/* Common Utilities Library        *            (C) Componentality Oy, 2015 */
/****************************************************************************/
/* Bit oriented operations                                                  */
/****************************************************************************/

#ifndef BITMEM_H
#define BITMEM_H

// Single bit type
typedef char bit;

// Callback type
typedef void (*BIT_CALLBACK)(void*);

// Bit-oriented ring buffer
typedef struct
{
	unsigned char* memory;			// Memory buffer allocated for the ring buffer
	unsigned long  size;			// Size of the buffer (in bits)
	unsigned long  free;			// Free space of the buffer (in bits)
	unsigned long  bit_read_ptr;	// Reading pointer (in bits)
	unsigned long  bit_write_ptr;	// Writing pointer (in bits)
	BIT_CALLBACK   overflow_cb;		// Callback on buffer overflow
	BIT_CALLBACK   underflow_cb;	// Callback on buffer underflow
} BIT_RINGBUF;

// *** Direct bit manipulations

// Retrieve one bit
bit bitmem_get1(unsigned char* mem, unsigned int index);
// Put one bit
void bitmem_put1(unsigned char* mem, unsigned int index, bit bitval);
// Retrieve given number of bits
unsigned long long bitmem_get(unsigned char* mem, unsigned int index, unsigned char numbits);
// Put given number of bits
void bitmem_put(unsigned char* mem, unsigned int index, unsigned long long val, unsigned char numbits);

// *** Ring buffer manipulations

// Ring buffer initialization
void bitmem_init_ringbuf(BIT_RINGBUF* brb, unsigned char* mem, unsigned long memsize_bytes, BIT_CALLBACK overflow_cb, BIT_CALLBACK underflow_cb);
// Write bits to ring buffer
void bitmem_write_ringbuf(BIT_RINGBUF* brb, unsigned long long val, unsigned char numbits);
// Read bits from the ring buffer
unsigned long long bitmem_read_ringbuf(BIT_RINGBUF* brb, unsigned char numbits);

#endif