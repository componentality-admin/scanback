#ifndef __HUFFMAN_H__
#define __HUFFMAN_H__

#include "bitmem.h"

struct HuffmanFrqTable
{
	unsigned long long mFrqTable[256];
	unsigned long long mTotalSymbolsProcessed;
};

struct HuffmanDescriptor
{
	unsigned short mStream[512 + 2];
	unsigned short mLength;
};

struct HuffmanBitString
{
	unsigned short mBitIndex;					// Index of the bit in a sequence
	unsigned char  mLength;						// Length of the string in bits
};

extern void huffmanFrqTableInit(struct HuffmanFrqTable*);
extern void huffmanFrqTableUpdate(struct HuffmanFrqTable*, const char*, const unsigned long long);
extern double huffmanGetFrequency(struct HuffmanFrqTable*, unsigned char);
extern void huffmanGetIndex(unsigned char[256], struct HuffmanFrqTable*);
extern void huffmanGetDescriptor(struct HuffmanFrqTable*, struct HuffmanDescriptor*);
extern void huffmanMakeRepresentation(struct HuffmanDescriptor*, struct HuffmanBitString[256], unsigned char* bitmem, unsigned short* bitsize);
extern void huffmanPutChar(const unsigned char, struct HuffmanBitString[256], unsigned char* bitsrc, unsigned char* bitdst, unsigned short* bitsize);
extern void huffmanPut(const unsigned char* array, const unsigned int array_size, struct HuffmanBitString[256], unsigned char* bitsrc, unsigned char* bitdst, unsigned short* bitsize);
extern void huffmanPutString(const char* array, struct HuffmanBitString[256], unsigned char* bitsrc, unsigned char* bitdst, unsigned short* bitsize);
extern unsigned char huffmanGetChar(struct HuffmanDescriptor*, unsigned char* bitmem, unsigned short* bitindex);
extern unsigned int huffmanGet(unsigned char* array, unsigned int maxsize, struct HuffmanDescriptor*, unsigned char* bitmem, unsigned short bitsize, unsigned short* bitindex);
extern unsigned int huffmanGetString(char* array, unsigned int maxsize, struct HuffmanDescriptor*, unsigned char* bitmem, unsigned short bitsize, unsigned short* bitindex);

#endif
