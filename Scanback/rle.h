/****************************************************************************/
/* Common Utilities Library        *            (C) Componentality Oy, 2015 */
/****************************************************************************/
/* RLE compression implementation                                           */
/****************************************************************************/
#ifndef __RLE_H__
#define __RLE_H__
#include <stdlib.h>

namespace Componentality
{
	namespace Common
	{
		// Do RLE compression
		void RLE_Encode(unsigned char* source, size_t source_size, unsigned char* target, size_t& target_size);
		// Do RLE decompression
		void RLE_Decode(unsigned char* source, size_t source_size, unsigned char* target, size_t& target_size);
		// Supplementary: Search for the position of the first different character in two memory buffers
		size_t isDiff(unsigned char* left, unsigned char* right, size_t size);

		// Incremental decoding structure
		typedef struct
		{
			unsigned char mMode;
			unsigned char mDecodedPortion;
			unsigned char mPtr;
			size_t mOffset;
			unsigned char* mSource;
		} RLE_DECODE;

		void RLE_InitDecoder(RLE_DECODE* handler, unsigned char* source);
		// Decode next byte incrementally
		unsigned char RLE_Fetch(RLE_DECODE* handler);

	} // namespace Common
} // namespace Componentality

#endif
