#include "huffman.h"

void huffmanFrqTableInit(struct HuffmanFrqTable* table)
{
	for (int i = 0; i < 256; i++)
		table->mFrqTable[i] = 0;
	table->mTotalSymbolsProcessed = 0;
}

void huffmanFrqTableUpdate(struct HuffmanFrqTable* table, const char* buffer, const unsigned long long size)
{
	for (unsigned long long i = 0; i < size; i++)
		table->mFrqTable[buffer[i]] += 1;
	table->mTotalSymbolsProcessed += size;
}

double huffmanGetFrequency(struct HuffmanFrqTable* table, unsigned char index)
{
	if (!table->mTotalSymbolsProcessed)
		return 0.0;
	return ((double)table->mFrqTable[index]) / ((double)table->mTotalSymbolsProcessed);
}

void huffmanGetIndex(unsigned char index[256], struct HuffmanFrqTable* table)
{
	for (int i = 0; i < 256; i++)
		index[i] = i;
	for (int i = 0; i < 256; i++)
		for (int j = 1; j < 256 - i; j++)
		{
			if (table->mFrqTable[index[j - 1]] < table->mFrqTable[index[j]])
			{
				unsigned char swap = index[j];
				index[j] = index[j - 1];
				index[j - 1] = swap;
			}
		}
}

unsigned short __huffmanProcessSubtable(
struct HuffmanFrqTable* table,
	unsigned char start,
	unsigned char end,
	double total,
	unsigned short* stream,
	unsigned short* stream_ptr,
	unsigned char* index
	)
{
	unsigned short stream_slot = *stream_ptr;
	double sum = 0.0;
	unsigned int bound;

	if (start == end)
	{
		return index[start] | 0x8000;
	}

	for (bound = start; bound <= end; bound++)
	{
		sum += huffmanGetFrequency(table, index[bound]);
		if (sum >= total / 2)
			break;
	}

	if ((start == 0) && (bound == 255))
		bound = 128;

	*stream_ptr += 2;
	
	stream[stream_slot] = __huffmanProcessSubtable(
		table,
		start,
		bound,
		sum,
		stream,
		stream_ptr,
		index
		);


	stream[stream_slot + 1] = __huffmanProcessSubtable(
		table,
		bound + 1,
		end,
		total - sum,
		stream,
		stream_ptr,
		index
		);

	return stream_slot;
}

void huffmanGetDescriptor(struct HuffmanFrqTable* table, struct HuffmanDescriptor* desc)
{
	unsigned char index[256];
	desc->mLength = 0;
	huffmanGetIndex(index, table);
	__huffmanProcessSubtable(table, 0, 255, 1.0, &desc->mStream[0], &desc->mLength, index);
}

void __copyToBitmem(unsigned char* bitmem, unsigned short index, char* string, unsigned long size)
{
	for (unsigned short i = 0; i < size; i++)
		bitmem_put1(bitmem, index + i, string[i] == '1' ? 1 : 0);
}

void __copyBitmem(unsigned char* bitmem, unsigned short index, unsigned char* bitsrc, unsigned short srcidx, unsigned short size)
{
	for (unsigned short i = 0; i < size; i++)
		bitmem_put1(bitmem, index + i, bitmem_get1(bitsrc, i + srcidx));
}

void __huffmanMakeRepresentation(
	struct HuffmanDescriptor* desc, 
	struct HuffmanBitString header[256], 
	unsigned char* bitmem,
	unsigned short* bitmem_used_size,
	char* current_string, 
	unsigned char current_string_size,
	unsigned short offset)
{
	if (desc->mStream[offset] & 0x8000)
	{
		__copyToBitmem(bitmem, *bitmem_used_size, current_string, current_string_size);
		bitmem_put1(bitmem, *bitmem_used_size + current_string_size, 0);
		header[desc->mStream[offset] & 0x7FFF].mBitIndex = *bitmem_used_size;
		header[desc->mStream[offset] & 0x7FFF].mLength = current_string_size + 1;
		*bitmem_used_size += current_string_size + 1;
	}
	else
	{
		current_string[current_string_size] = '0';
		__huffmanMakeRepresentation(desc, header, bitmem, bitmem_used_size, current_string, current_string_size + 1, desc->mStream[offset]);
	}

	if (desc->mStream[offset + 1] & 0x8000)
	{
		__copyToBitmem(bitmem, *bitmem_used_size, current_string, current_string_size);
		bitmem_put1(bitmem, *bitmem_used_size + current_string_size, 1);
		header[desc->mStream[offset + 1] & 0x7FFF].mBitIndex = *bitmem_used_size;
		header[desc->mStream[offset + 1] & 0x7FFF].mLength = current_string_size + 1;
		*bitmem_used_size += current_string_size + 1;
	}
	else
	{
		current_string[current_string_size] = '1';
		__huffmanMakeRepresentation(desc, header, bitmem, bitmem_used_size, current_string, current_string_size + 1, desc->mStream[offset + 1]);
	}
}

void huffmanMakeRepresentation(struct HuffmanDescriptor* desc, struct HuffmanBitString header[256], unsigned char* bitmem, unsigned short* bitsize)
{
	char string[256];
	__huffmanMakeRepresentation(desc, header, bitmem, bitsize, string, 0, 0);
}

void huffmanPutChar(const unsigned char symbol, struct HuffmanBitString header[256], unsigned char* bitsrc, unsigned char* bitdst, unsigned short* bitsize)
{
	if (bitdst)
		__copyBitmem(bitdst, *bitsize, bitsrc, header[symbol].mBitIndex, header[symbol].mLength);
	*bitsize += header[symbol].mLength;
}

void huffmanPut(const unsigned char* array, const unsigned int array_size, struct HuffmanBitString header[256], unsigned char* bitsrc, unsigned char* bitdst, unsigned short* bitsize)
{
	for (unsigned int i = 0; i < array_size; i++)
		huffmanPutChar(array[i], header, bitsrc, bitdst, bitsize);
}

void huffmanPutString(const char* array, struct HuffmanBitString header[256], unsigned char* bitsrc, unsigned char* bitdst, unsigned short* bitsize)
{
	for (unsigned int i = 0; array[i]; i++)
		huffmanPutChar((unsigned char) array[i], header, bitsrc, bitdst, bitsize);
}

unsigned char huffmanGetChar(struct HuffmanDescriptor* desc, unsigned char* bitmem, unsigned short* bitindex)
{
	unsigned short offset = bitmem_get1(bitmem, *bitindex);
	unsigned short item = desc->mStream[offset];
	while ((item & 0x8000) == 0)
	{
		*bitindex += 1;
		if (bitmem_get1(bitmem, *bitindex))
			offset = desc->mStream[offset] + 1;
		else
			offset = desc->mStream[offset];
		item = desc->mStream[offset];
	};
	*bitindex += 1;
	return (unsigned char)(item & 0x7FFF);
}

unsigned int huffmanGet(unsigned char* array, unsigned int maxsize, struct HuffmanDescriptor* desc, unsigned char* bitmem, unsigned short bitsize, unsigned short* bitindex) 
{
	unsigned int i = 0;
	while ((*bitindex < bitsize) && (i < maxsize))
	{
		array[i++] = huffmanGetChar(desc, bitmem, bitindex);
	}
	return i;
}

unsigned int huffmanGetString(char* array, unsigned int maxsize, struct HuffmanDescriptor* desc, unsigned char* bitmem, unsigned short bitsize, unsigned short* bitindex)
{
	unsigned int result = huffmanGet((unsigned char*)array, maxsize - 1, desc, bitmem, bitsize, bitindex);
	array[result] = 0;
	return result;
}
